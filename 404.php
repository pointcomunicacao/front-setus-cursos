<?php include('header.php'); ?>

<section class="title-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 data-anime="top">Página não encontrada</h1>
            </div>
        </div>
    </div>
</section>

<section class="page404">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <!-- 

                APAGAR APÓS APLICAR A REGRA

                Aldo, nesse search abaixo o usuário digita o que quer e clica no botão e vai para a página search.. blx..
                Se tiver como colocar o h2 para ser editado lá no admin em settings.. seria  ideal

                -->

                <h2>Não foi possível localizar o que estava buscando, busque abaixo pelos nossos cursos.</h2>
                <form action="" class="form-inline d-flex justify-content-center">
                    <input type="search" class="form-control" placeholder="Busque um Curso ou Categoria">
                    <button class="btn-acessar" type="submit">
                        <i class="fas fa-search"></i>
                    </button>
                </form>
            </div>
        </div>
    </div>
</section>


<?php include('footer.php'); ?>