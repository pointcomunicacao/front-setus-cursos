<?php include('header.php'); ?>

<section class="title-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 data-anime="top">Validar Certificado</h1>
            </div>
        </div>
    </div>
</section>

<section class="validar-certificado">
    <div class="container">
        <div class="row">
            <div class="box-validar-certificado">
                <div class="col-sm-6 offset-sm-3">
                    <p>
                        Mussum Ipsum, cacilds vidis litro abertis. In elementis mé pra quem é amistosis quis leo. Paisis, filhis, espiritis santis. Praesent vel viverra nisi. Mauris aliquet nunc non turpis scelerisque, eget. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.
                    </p>

                    <form action="">
                        <input type="text" class="form-control" placeholder="Digite o número do certificado">
                        <span>Digite o número do certificado para verificar se é válido ou não</span>
                        <button type="submit" class="btn-acessar">
                            Validar
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


<?php include('footer.php'); ?>