</div><!-- PAGE CONTENT -->

<div class="divisor"></div>
<footer id="sticky-footer">
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <ul class="text-center menu-rodape">
                    <li>
                        <a href="">Validar Certificado</a>
                    </li>

                    <li>
                        <a href="">Termos de Serviço</a>
                    </li>

                    <li>
                        <a href="">Política de Privacidade</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <p>&copy; <?php echo date("Y"); ?> - Todos os direitos reservados - Setus Cursos</p>
                <a href="https://pointcomunicacao.ppg.br" 
                target="_blank" title="Point Comunicacao Web" 
                alt="Point Comunicacao Web"
                class="link-footer">
                    Por Point Comunicação
                </a>
            </div>
        </div>

        
    </div>
</footer>

<a href="https://api.whatsapp.com/send?phone=5511990244340" class="whatsapp-chat" target="_blank">
    <img src="assets/img/whatsapp.png">
</a>

<a href="javascript:" id="return-to-top"><i class="fas fa-chevron-up"></i></a>
<script src="assets/js/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/pointcom.min.js"></script>

</body>

</html>