<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <title>Setus Cursos</title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Wesley Oliveira - Point Comunicação - https://pointcomunicacao.ppg.br">
    <link rel="icon" type="image/png" href="assets/img/favicon.png">
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="96x96">
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="192x192">

    <!-- FAVICON PARA APPLE -->
    <link rel="apple-touch-icon" href="assets/img/favicon.png" sizes="57x57">
    <link rel="apple-touch-icon" href="assets/img/favicon.png" sizes="60x60">
    <link rel="apple-touch-icon" href="assets/img/favicon.png" sizes="72x72">
    <link rel="apple-touch-icon" href="assets/img/favicon.png" sizes="76x76">
    <link rel="apple-touch-icon" href="assets/img/favicon.png" sizes="114x114">
    <link rel="apple-touch-icon" href="assets/img/favicon.png" sizes="144x144">
    <link rel="apple-touch-icon" href="assets/img/favicon.png" sizes="152x152">
    <link rel="apple-touch-icon" href="assets/img/favicon.png" sizes="180x180">

    <!-- FONTES -->
    <link rel="stylesheet" href="assets/fontawsome/css/all.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!-- Folha de Estilo -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/pointcom.css">

</head>

<body>
    <div id="page-content">

        <header>
            <div class="subheader">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 box-subheader">
                            <a href="contato.php" class="link">
                                Fale Conosco
                            </a>
                        </div>

                        <div class="col-sm-6 box-subheader">
                            <form class="form-inline d-flex formulario-header">
                                <button type="submit" class="btn-gplus mb-2">
                                    Acessar com <i class="fab fa-google-plus-g"></i>
                                </button>

                                <button type="submit" class="btn-facebook mb-2">
                                    Acessar com <i class="fab fa-facebook-f"></i>
                                </button>

                                <button type="submit" class="btn-acessar mb-2">
                                    Acessar / Registrar
                                </button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>


            <div class="cabecalho">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-2">
                            <a href="index.php">
                                <div class="logotipo">
                                    <img src="assets/img/logotipo-setus-cursos.png" alt="" class="img-fluid">
                                </div>
                            </a>
                        </div>
                        <!--
                            Se estiver logado
                        
                        <div class="col-sm-10 box-logado">
                            <p>
                                Olá,
                                <a href="" class="pointcom-link">Wesley Oliveira</a>
                                <a href="" class="pointcom-link-close">
                                    <i class="fas fa-sign-out-alt"></i>
                                </a>
                            </p>
                        </div>
                            Senão logado... 
                        -->


                        <!--
                            APÓS APLICAR APAGAR OS COMENTÁRIOS DESNECESSÁRIOS
                            NÃO ESQUEÇA DOS ALERTS
                        -->
                        <div class="col-sm-10 hide-mobile">
                            <form class="form-inline d-flex justify-content-end form-header hide-mobile">
                                <div class="form-group mb-2">
                                    <input type="text" class="form-control" placeholder="Usuário">
                                </div>
                                <div class="form-group mx-sm-3 mb-2">
                                    <input type="pass" class="form-control" placeholder="Senha">
                                </div>
                                <button type="submit" class="btn-acessar mb-2">Acessar</button>
                                <!-- 
                                <button type="submit" class="mb-2 btn-gplus">
                                    Acessar com <i class="fab fa-google-plus-g"></i>
                                </button>

                                <button type="submit" class="mb-2 btn-facebook">
                                    Acessar com <i class="fab fa-facebook-f"></i>
                                </button>
                                -->
                            </form>
                        </div>

                    </div>
                </div>
            </div>

            <div class="menuPrincipal">
                <div class="container">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#setuscursosmenu" aria-controls="setuscursosmenu" aria-expanded="false" aria-label="Toggle navigation">
                            <i class="material-icons">sort</i>
                        </button>

                        <div class="collapse navbar-collapse" id="setuscursosmenu">
                            <ul class="navbar-nav mr-auto pointcom-menu">

                                <li class="nav-item active">
                                    <!-- NÃO ESQUEÇA DA VALIDAÇÃO SE ESTIVER ATIVO PRINTA A CLASS ACTIVE -->
                                    <a class="nav-link" href="index.php">Home
                                        <span class="sr-only">(current)</span>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="nossos-cursos.php">Cursos</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="certificado.php">Certificados</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>

        </header>