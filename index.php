<?php include('header.php'); ?>

<section class="busca-cursos">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <form action="" class="pointcom-formulario">
                    <div class="col-sm-6 offset-sm-3">
                        <input type="search" class="form-control" placeholder="Busque um Curso ou Categoria">
                        <button class="btn-padrao btn-search" type="submit">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>

                    <div class="col-sm-6 offset-sm-3">
                        <div class="result-search">
                            <ul>
                                <li><a href="">Resultado 1</a></li>
                                <li><a href="">Resultado 2</a></li>
                                <li><a href="">Resultado 3</a></li>
                                <li><a href="">Resultado 4</a></li>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<div class="divisor"></div>

<section class="produtos-list">
    <?php include('produtos.php'); ?>
</section>

<?php include('footer.php'); ?>