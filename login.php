<?php include('header.php') ?>

<section class="title-page" data-anime="bottom">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Acesse sua área de aluno</h1>
            </div>
        </div>
    </div>
</section>

<section class="content-login">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="formularios" data-anime="left">
                    <h1 class="titulos">Acessar</h1>
                    <p>Se você já possui uma conta conosco, basta apenas se logar em uma das opções abaixo.</p>
                    <form action="">
                        <input type="text" class="form-control" placeholder="Usuário">
                        <input type="pass" class="form-control" placeholder="Senha">
                        <button type="submit" class="btn-acessar">Acessar</button>
                        <button type="submit" class="btn-acessar btn-gplus">
                            <i class="fab fa-google-plus-g"></i>
                        </button>

                        <button type="submit" class="btn-acessar btn-facebook">
                            <i class="fab fa-facebook-f"></i>
                        </button>
                    </form>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="formularios" data-anime="right">
                    <h1 class="titulos">Registar-se</h1>
                    <p>Ainda não tem uma conta conosco? registre-se agora mesmo.</p>
                    <form action="">
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" class="form-control" placeholder="Nome Completo">
                                <input type="text" class="form-control" placeholder="E-mail">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="text" class="form-control" placeholder="Senha">
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" placeholder="Confirme a Senha">
                            </div>
                        </div>
                        <button type="submit" class="btn-acessar">Registrar</button>
                        <button type="submit" class="btn-acessar btn-gplus">
                            <i class="fab fa-google-plus-g"></i>
                        </button>

                        <button type="submit" class="btn-acessar btn-facebook">
                            <i class="fab fa-facebook-f"></i>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include('footer.php') ?>