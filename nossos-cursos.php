<?php include('header.php'); ?>

<section class="title-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 data-anime="top">Nossos Cursos</h1>
            </div>
        </div>
    </div>
</section>

<section class="list-course-category">
    <div class="container">
        <div class="row">

            <!-- sidebar -->
            <div class="col-sm-3">
                <div class="sidebar-category">
                    <h1 class="title-sidebar-category">Categorias</h1>

                    <ul>
                        <li><a href="">1</a></li><!-- no (a) possui a class="ativo" -->
                        <li><a href="" class="ativo">2</a></li>
                        <li><a href="">3</a></li>
                        <li><a href="">4</a></li>
                        <li><a href="">5</a></li>
                    </ul>

                </div>
            </div>

            <!-- content -->
            <div class="col-sm-9">

                <!-- loop -->
                <div class="content-category">

                    <div class="row">
                        <div class="col-sm-5">
                            <a href="#">
                                <img src="assets/img/pnl.jpg" alt="" class="img-fluid">
                            </a>
                        </div>

                        <div class="col-sm-7">
                            <div class="box-info-course-category">
                                <a href="#">
                                    <h1>Título do Curso</h1>
                                </a>
                                <p class="reviews">
                                    <span><i class="fas fa-star"></i></span>
                                    <span><i class="fas fa-star"></i></span>
                                    <span><i class="fas fa-star"></i></span>
                                    <span><i class="fas fa-star"></i></span>
                                    <span><i class="far fa-star"></i></span>
                                </p>

                                <div class="description-course">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer tempor ligula ornare, viverra libero sed, lobortis metus.
                                </div>

                                <div class="box-btn">

                                    <!-- start:: Botão de Ação conforme curso do aluno -->
                                    <a href="single-cursos.php" class="btn-acessar btn-sucesso">Solicitar Certificado</a>
                                    <!-- <a href="single-cursos.php" class="btn-acessar btn-inverse">Concluir Curso</a> -->
                                    <!-- end:: Botão de Ação conforme curso do aluno -->

                                    <a href="single-cursos.php" class="btn-acessar">Acessar Curso</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //loop -->

               <!-- loop -->
                <div class="content-category">

                    <div class="row">
                        <div class="col-sm-5">
                            <a href="#">
                                <img src="assets/img/pnl.jpg" alt="" class="img-fluid">
                            </a>
                        </div>

                        <div class="col-sm-7">
                            <div class="box-info-course-category">
                                <a href="#">
                                    <h1>Título do Curso</h1>
                                </a>
                                <p class="reviews">
                                    <span><i class="fas fa-star"></i></span>
                                    <span><i class="fas fa-star"></i></span>
                                    <span><i class="fas fa-star"></i></span>
                                    <span><i class="fas fa-star"></i></span>
                                    <span><i class="far fa-star"></i></span>
                                </p>

                                <div class="description-course">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer tempor ligula ornare, viverra libero sed, lobortis metus.
                                </div>

                                <div class="box-btn">

                                    <!-- start:: Botão de Ação conforme curso do aluno -->
                                    <a href="single-cursos.php" class="btn-acessar btn-sucesso">Solicitar Certificado</a>
                                    <!-- <a href="single-cursos.php" class="btn-acessar btn-inverse">Concluir Curso</a> -->
                                    <!-- end:: Botão de Ação conforme curso do aluno -->

                                    <a href="single-cursos.php" class="btn-acessar">Acessar Curso</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //loop -->

                <!-- loop -->
                <div class="content-category">

                    <div class="row">
                        <div class="col-sm-5">
                            <a href="#">
                                <img src="assets/img/pnl.jpg" alt="" class="img-fluid">
                            </a>
                        </div>

                        <div class="col-sm-7">
                            <div class="box-info-course-category">
                                <a href="#">
                                    <h1>Título do Curso</h1>
                                </a>
                                <p class="reviews">
                                    <span><i class="fas fa-star"></i></span>
                                    <span><i class="fas fa-star"></i></span>
                                    <span><i class="fas fa-star"></i></span>
                                    <span><i class="fas fa-star"></i></span>
                                    <span><i class="far fa-star"></i></span>
                                </p>

                                <div class="description-course">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer tempor ligula ornare, viverra libero sed, lobortis metus.
                                </div>

                                <div class="box-btn">

                                    <!-- start:: Botão de Ação conforme curso do aluno -->
                                    <a href="single-cursos.php" class="btn-acessar btn-sucesso">Solicitar Certificado</a>
                                    <!-- <a href="single-cursos.php" class="btn-acessar btn-inverse">Concluir Curso</a> -->
                                    <!-- end:: Botão de Ação conforme curso do aluno -->

                                    <a href="single-cursos.php" class="btn-acessar">Acessar Curso</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //loop -->

                <!-- loop -->
                <div class="content-category">

                    <div class="row">
                        <div class="col-sm-5">
                            <a href="#">
                                <img src="assets/img/pnl.jpg" alt="" class="img-fluid">
                            </a>
                        </div>

                        <div class="col-sm-7">
                            <div class="box-info-course-category">
                                <a href="#">
                                    <h1>Título do Curso</h1>
                                </a>
                                <p class="reviews">
                                    <span><i class="fas fa-star"></i></span>
                                    <span><i class="fas fa-star"></i></span>
                                    <span><i class="fas fa-star"></i></span>
                                    <span><i class="fas fa-star"></i></span>
                                    <span><i class="far fa-star"></i></span>
                                </p>

                                <div class="description-course">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer tempor ligula ornare, viverra libero sed, lobortis metus.
                                </div>

                                <div class="box-btn">

                                    <!-- start:: Botão de Ação conforme curso do aluno -->
                                    <a href="single-cursos.php" class="btn-acessar btn-sucesso">Solicitar Certificado</a>
                                    <!-- <a href="single-cursos.php" class="btn-acessar btn-inverse">Concluir Curso</a> -->
                                    <!-- end:: Botão de Ação conforme curso do aluno -->

                                    <a href="single-cursos.php" class="btn-acessar">Acessar Curso</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //loop -->


            </div>
        </div>
    </div>
</section>

<?php include('footer.php'); ?>