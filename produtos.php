<?php

/**
 * Aldo a lógica aplicada nos box abaixo é
 * se existir um bloco, printa o data-anime(left)
 *
 * se existir dois blocos, printa o data-anime(left) no primeiro bloco e no segundo bloco printa o
 * data-anime(bottom)
 *
 * se existir 3 blocos, printa o data-anime(left) no primeiro, no segundo o data-anime(bottom) e o terceiro
 * printa o data-anime(right)
 *
 * POR GENTILEZA APÓS COMPREENDER A LÓGICA APAGAR ESSE COMENTÁRIO
 *
 * NO LOOP LISTAR 15 POSTS
 *
 * NÃO ESQUEÇA, NO ADMIN TERA UMA ÁREA PARA ELE ESCOLHER ENTRE OS
 *
 * ( MAIS VISTOS, MAIS COMPRADOS, ULTIMOS POSTS E DESTAQUES (DESTACADO) )
 *
 */
?>


<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h1 class="titulos">Cursos Online Gratuitos<span> Com Certificados</span></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4" data-anime="left">
            <div class="box-cursos">
                <div class="box-imagem-cursos">
                    <a href="single-cursos.php">
                        <!-- 
                            **** EXCLUIR O COMENTÁRIO APÓS LER: ****
                            TENTA FAZER O CROP DAS IMAGENS PARA NÃO ESTOURAR POR GENTILEZA
                        -->
                        <img src="assets/img/pnl.jpg" alt="" class="img-fluid">
                    </a>
                    <span>curso grátis</span>
                    <p class="clock-course"><i class="far fa-clock"></i> 30 hrs</p>
                </div>
                <div class="selo"><img src="assets/img/verified.png" alt="" class="img-fluid"></div>

                <div class="box-content-cursos">
                    <a href="" class="categorias">Saúde</a>
                    <a href="single-cursos.php">
                        <h2>PNL - Programação Neurolinguística</h2>
                    </a>
                </div>
                <div class="footer-box-cursos">
                    <a href="" class="btn-acessar">Acessar</a>
                </div>
            </div>
        </div>

        <div class="col-sm-4" data-anime="bottom">
            <div class="box-cursos">
                <div class="box-imagem-cursos">
                    <a href="single-cursos.php">
                        <img src="assets/img/etiqueta.jpg" alt="" class="img-fluid">
                    </a>
                    <span>curso grátis</span>
                    <p class="clock-course"><i class="far fa-clock"></i> 30 hrs</p>
                </div>
                <div class="selo"><img src="assets/img/verified.png" alt="" class="img-fluid"></div>

                <div class="box-content-cursos">
                    <a href="" class="categorias">Educação</a>
                    <a href="single-cursos.php">
                        <h2>Curso de Etiqueta</h2>
                    </a>
                </div>
                <div class="footer-box-cursos">
                    <a href="" class="btn-acessar">Acessar</a>
                </div>
            </div>
        </div>

        <div class="col-sm-4" data-anime="right">
            <div class="box-cursos">
                <div class="box-imagem-cursos">
                    <a href="single-cursos.php">
                        <img src="assets/img/curso-de-logistica.jpg" alt="" class="img-fluid">
                    </a>
                    <span>curso grátis</span>
                    <p class="clock-course"><i class="far fa-clock"></i> 30 hrs</p>
                </div>
                <div class="selo"><img src="assets/img/verified.png" alt="" class="img-fluid"></div>

                <div class="box-content-cursos">
                    <a href="" class="categorias">Administração</a>
                    <a href="single-cursos.php">
                        <h2>Curso de Logística</h2>
                    </a>
                </div>
                <div class="footer-box-cursos">
                    <a href="" class="btn-acessar">Acessar</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <!-- REMOVER -->
        <div class="col-sm-4" data-anime="left">
            <div class="box-cursos">
                <div class="box-imagem-cursos">
                    <a href="single-cursos.php">
                        <img src="assets/img/pnl.jpg" alt="" class="img-fluid">
                    </a>
                    <span>curso grátis</span>
                    <p class="clock-course"><i class="far fa-clock"></i> 30 hrs</p>
                </div>
                <div class="selo"><img src="assets/img/verified.png" alt="" class="img-fluid"></div>

                <div class="box-content-cursos">
                    <a href="" class="categorias">Saúde</a>
                    <a href="single-cursos.php">
                        <h2>PNL - Programação Neurolinguística</h2>
                    </a>
                </div>
                <div class="footer-box-cursos">
                    <a href="" class="btn-acessar">Acessar</a>
                </div>
            </div>
        </div>

        <div class="col-sm-4" data-anime="bottom">
            <div class="box-cursos">
                <div class="box-imagem-cursos">
                    <a href="single-cursos.php">
                        <img src="assets/img/etiqueta.jpg" alt="" class="img-fluid">
                    </a>
                    <span>curso grátis</span>
                    <p class="clock-course"><i class="far fa-clock"></i> 30 hrs</p>
                </div>
                <div class="selo"><img src="assets/img/verified.png" alt="" class="img-fluid"></div>

                <div class="box-content-cursos">
                    <a href="" class="categorias">Educação</a>
                    <a href="single-cursos.php">
                        <h2>Curso de Etiqueta</h2>
                    </a>
                </div>
                <div class="footer-box-cursos">
                    <a href="" class="btn-acessar">Acessar</a>
                </div>
            </div>
        </div>

        <div class="col-sm-4" data-anime="right">
            <div class="box-cursos">
                <div class="box-imagem-cursos">
                    <a href="single-cursos.php">
                        <img src="assets/img/curso-de-logistica.jpg" alt="" class="img-fluid">
                    </a>
                    <span>curso grátis</span>
                    <p class="clock-course"><i class="far fa-clock"></i> 30 hrs</p>
                </div>
                <div class="selo"><img src="assets/img/verified.png" alt="" class="img-fluid"></div>

                <div class="box-content-cursos">
                    <a href="" class="categorias">Administração</a>
                    <a href="single-cursos.php">
                        <h2>Curso de Logística</h2>
                    </a>
                </div>
                <div class="footer-box-cursos">
                    <a href="" class="btn-acessar">Acessar</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <!-- REMOVER -->
        <div class="col-sm-4" data-anime="left">
            <div class="box-cursos">
                <div class="box-imagem-cursos">
                    <a href="single-cursos.php">
                        <img src="assets/img/pnl.jpg" alt="" class="img-fluid">
                    </a>
                    <span>curso grátis</span>
                    <p class="clock-course"><i class="far fa-clock"></i> 30 hrs</p>
                </div>
                <div class="selo"><img src="assets/img/verified.png" alt="" class="img-fluid"></div>

                <div class="box-content-cursos">
                    <a href="" class="categorias">Saúde</a>
                    <a href="single-cursos.php">
                        <h2>PNL - Programação Neurolinguística</h2>
                    </a>
                </div>
                <div class="footer-box-cursos">
                    <a href="" class="btn-acessar">Acessar</a>
                </div>
            </div>
        </div>

        <div class="col-sm-4" data-anime="bottom">
            <div class="box-cursos">
                <div class="box-imagem-cursos">
                    <a href="single-cursos.php">
                        <img src="assets/img/etiqueta.jpg" alt="" class="img-fluid">
                    </a>
                    <span>curso grátis</span>
                    <p class="clock-course"><i class="far fa-clock"></i> 30 hrs</p>
                </div>
                <div class="selo"><img src="assets/img/verified.png" alt="" class="img-fluid"></div>

                <div class="box-content-cursos">
                    <a href="" class="categorias">Educação</a>
                    <a href="single-cursos.php">
                        <h2>Curso de Etiqueta</h2>
                    </a>
                </div>
                <div class="footer-box-cursos">
                    <a href="" class="btn-acessar">Acessar</a>
                </div>
            </div>
        </div>

        <div class="col-sm-4" data-anime="right">
            <div class="box-cursos">
                <div class="box-imagem-cursos">
                    <a href="single-cursos.php">
                        <img src="assets/img/curso-de-logistica.jpg" alt="" class="img-fluid">
                    </a>
                    <span>curso grátis</span>
                    <p class="clock-course"><i class="far fa-clock"></i> 30 hrs</p>
                </div>
                <div class="selo"><img src="assets/img/verified.png" alt="" class="img-fluid"></div>

                <div class="box-content-cursos">
                    <a href="" class="categorias">Administração</a>
                    <a href="single-cursos.php">
                        <h2>Curso de Logística</h2>
                    </a>
                </div>
                <div class="footer-box-cursos">
                    <a href="" class="btn-acessar">Acessar</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <!-- REMOVER -->
        <div class="col-sm-4" data-anime="left">
            <div class="box-cursos">
                <div class="box-imagem-cursos">
                    <a href="single-cursos.php">
                        <img src="assets/img/pnl.jpg" alt="" class="img-fluid">
                    </a>
                    <span>curso grátis</span>
                    <p class="clock-course"><i class="far fa-clock"></i> 30 hrs</p>
                </div>
                <div class="selo"><img src="assets/img/verified.png" alt="" class="img-fluid"></div>

                <div class="box-content-cursos">
                    <a href="" class="categorias">Saúde</a>
                    <a href="single-cursos.php">
                        <h2>PNL - Programação Neurolinguística</h2>
                    </a>
                </div>
                <div class="footer-box-cursos">
                    <a href="" class="btn-acessar">Acessar</a>
                </div>
            </div>
        </div>

        <div class="col-sm-4" data-anime="bottom">
            <div class="box-cursos">
                <div class="box-imagem-cursos">
                    <a href="single-cursos.php">
                        <img src="assets/img/etiqueta.jpg" alt="" class="img-fluid">
                    </a>
                    <span>curso grátis</span>
                    <p class="clock-course"><i class="far fa-clock"></i> 30 hrs</p>
                </div>
                <div class="selo"><img src="assets/img/verified.png" alt="" class="img-fluid"></div>

                <div class="box-content-cursos">
                    <a href="" class="categorias">Educação</a>
                    <a href="single-cursos.php">
                        <h2>Curso de Etiqueta</h2>
                    </a>
                </div>
                <div class="footer-box-cursos">
                    <a href="" class="btn-acessar">Acessar</a>
                </div>
            </div>
        </div>

        <div class="col-sm-4" data-anime="right">
            <div class="box-cursos">
                <div class="box-imagem-cursos">
                    <a href="single-cursos.php">
                        <img src="assets/img/curso-de-logistica.jpg" alt="" class="img-fluid">
                    </a>
                    <span>curso grátis</span>
                    <p class="clock-course"><i class="far fa-clock"></i> 30 hrs</p>
                </div>
                <div class="selo"><img src="assets/img/verified.png" alt="" class="img-fluid"></div>

                <div class="box-content-cursos">
                    <a href="" class="categorias">Administração</a>
                    <a href="single-cursos.php">
                        <h2>Curso de Logística</h2>
                    </a>
                </div>
                <div class="footer-box-cursos">
                    <a href="" class="btn-acessar">Acessar</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <!-- REMOVER -->
        <div class="col-sm-4" data-anime="left">
            <div class="box-cursos">
                <div class="box-imagem-cursos">
                    <a href="single-cursos.php">
                        <img src="assets/img/pnl.jpg" alt="" class="img-fluid">
                    </a>
                    <span>curso grátis</span>
                    <p class="clock-course"><i class="far fa-clock"></i> 30 hrs</p>
                </div>
                <div class="selo"><img src="assets/img/verified.png" alt="" class="img-fluid"></div>

                <div class="box-content-cursos">
                    <a href="" class="categorias">Saúde</a>
                    <a href="single-cursos.php">
                        <h2>PNL - Programação Neurolinguística</h2>
                    </a>
                </div>
                <div class="footer-box-cursos">
                    <a href="" class="btn-acessar">Acessar</a>
                </div>
            </div>
        </div>

        <div class="col-sm-4" data-anime="bottom">
            <div class="box-cursos">
                <div class="box-imagem-cursos">
                    <a href="single-cursos.php">
                        <img src="assets/img/etiqueta.jpg" alt="" class="img-fluid">
                    </a>
                    <span>curso grátis</span>
                    <p class="clock-course"><i class="far fa-clock"></i> 30 hrs</p>
                </div>
                <div class="selo"><img src="assets/img/verified.png" alt="" class="img-fluid"></div>

                <div class="box-content-cursos">
                    <a href="" class="categorias">Educação</a>
                    <a href="single-cursos.php">
                        <h2>Curso de Etiqueta</h2>
                    </a>
                </div>
                <div class="footer-box-cursos">
                    <a href="" class="btn-acessar">Acessar</a>
                </div>
            </div>
        </div>

        <div class="col-sm-4" data-anime="right">
            <div class="box-cursos">
                <div class="box-imagem-cursos">
                    <a href="single-cursos.php">
                        <img src="assets/img/curso-de-logistica.jpg" alt="" class="img-fluid">
                    </a>
                    <span>curso grátis</span>
                    <p class="clock-course"><i class="far fa-clock"></i> 30 hrs</p>
                </div>
                <div class="selo"><img src="assets/img/verified.png" alt="" class="img-fluid"></div>

                <div class="box-content-cursos">
                    <a href="" class="categorias">Administração</a>
                    <a href="single-cursos.php">
                        <h2>Curso de Logística</h2>
                    </a>
                </div>
                <div class="footer-box-cursos">
                    <a href="" class="btn-acessar">Acessar</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="box-btn-vermais">
                <a href="" class="btn-acessar">
                    Ver mais Cursos
                </a>
            </div>
        </div>
    </div>
</div>