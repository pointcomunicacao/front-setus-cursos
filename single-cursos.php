<?php include('header.php') ?>

<section class="title-page-course" data-anime="bottom">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>{Curso de} Nome do curso {Online Grátis}</h1>
                <ul>
                    <li><a href="index.php">HOME</a></li>
                    <li><a href="">Categoria</a></li>
                    <li>Nome do Curso</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="content-course">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <div class="box-title-course" data-anime="left">
                    <div class="row">
                        <div class="col-sm-4">
                            <h1>Categoria</h1>
                            <p>Administração</p>
                        </div>

                        <div class="col-sm-4">
                            <h1>Alunos</h1>
                            <p>18</p><!-- Será preenchido fake (no admin), se chegar no valor preenchido ele retorna o valor real -->
                        </div>

                        <div class="col-sm-4">
                            <a href="#" class="btn-acessar btn-front">
                                Iniciar curso
                            </a>
                        </div>

                        
                    </div>
                </div>


                <div class="content">
                    <div class="row">
                        <div class="col-sm-12">

                            <div class="imagem" data-anime="top">
                                <img src="assets/img/curso-teste-single.jpg" alt="" class="img-fluid">
                            </div>

                            <div class="box-conteudo" data-anime="right">
                                <ul class="nav nav-tabs pointcom-tab" id="pointcomTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="descricao-tab" data-toggle="tab" href="#descricao" role="tab" aria-controls="descricao" aria-selected="true">
                                            <span class="arrow">
                                                <i class="fas fa-long-arrow-alt-right"></i>
                                            </span>
                                            Sobre o Curso
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="gradecurso-tab" data-toggle="tab" href="#gradecurso" role="tab" aria-controls="gradecurso" aria-selected="false">
                                            <span class="arrow">
                                                <i class="fas fa-long-arrow-alt-right"></i>
                                            </span>
                                            Conteúdo
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="avaliacao-tab" data-toggle="tab" href="#avaliacao" role="tab" aria-controls="avaliacao" aria-selected="false">
                                            <span class="arrow">
                                                <i class="fas fa-long-arrow-alt-right"></i>
                                            </span>
                                            Comentários e Avaliações
                                        </a>
                                    </li>
                                </ul>

                                <div class="tab-content pointcom-tab-content" id="pointcomTabContent">
                                    <div class="tab-pane fade show active" id="descricao" role="tabpanel" aria-labelledby="descricao-tab">
                                        
                                        <p>
                                            Mussum Ipsum, cacilds vidis litro abertis. Aenean aliquam molestie leo, vitae iaculis nisl. Não sou faixa preta cumpadi, sou preto inteiris, inteiris. Leite de capivaris, leite de mula manquis sem cabeça. Diuretics paradis num copo é motivis de denguis.

                                            Mais vale um bebadis conhecidiss, que um alcoolatra anonimis. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis. Si num tem leite então bota uma pinga aí cumpadi! Sapien in monti palavris qui num significa nadis i pareci latim.
                                            <br>
                                            <br>
                                            Mussum Ipsum, cacilds vidis litro abertis. Aenean aliquam molestie leo, vitae iaculis nisl. Não sou faixa preta cumpadi, sou preto inteiris, inteiris. Leite de capivaris, leite de mula manquis sem cabeça. Diuretics paradis num copo é motivis de denguis.

                                            Mais vale um bebadis conhecidiss, que um alcoolatra anonimis. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis. Si num tem leite então bota uma pinga aí cumpadi! Sapien in monti palavris qui num significa nadis i pareci latim.
                                        </p>
                                    </div>
                                    <div class="tab-pane fade" id="gradecurso" role="tabpanel" aria-labelledby="gradecurso-tab">
                                        
                                        <p>
                                            Mussum Ipsum, cacilds vidis litro abertis. Aenean aliquam molestie leo, vitae iaculis nisl. Não sou faixa preta cumpadi, sou preto inteiris, inteiris. Leite de capivaris, leite de mula manquis sem cabeça. Diuretics paradis num copo é motivis de denguis.

                                            Mais vale um bebadis conhecidiss, que um alcoolatra anonimis. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis. Si num tem leite então bota uma pinga aí cumpadi! Sapien in monti palavris qui num significa nadis i pareci latim.
                                        </p>
                                        <ul>
                                            <li>
                                                <a href="">1</a>
                                                <a href="">2</a>
                                                <a href="">3</a>
                                                <a href="">4</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="tab-pane fade" id="avaliacao" role="tabpanel" aria-labelledby="avaliacao-tab">
                                        
                                        <p>
                                            Mussum Ipsum, cacilds vidis litro abertis. Aenean aliquam molestie leo, vitae iaculis nisl. Não sou faixa preta cumpadi, sou preto inteiris, inteiris. Leite de capivaris, leite de mula manquis sem cabeça. Diuretics paradis num copo é motivis de denguis.
                                        </p>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="box-value-reviews">
                                                    <h1>4.9</h1>
                                                    <span class="reviews-info">de 5 estrelas</span>

                                                    <div class="stars">
                                                        <span><i class="fas fa-star"></i></span>
                                                        <span><i class="fas fa-star"></i></span>
                                                        <span><i class="fas fa-star"></i></span>
                                                        <span><i class="fas fa-star"></i></span>
                                                        <span><i class="far fa-star"></i></span>
                                                    </div>

                                                    <div class="reviews">
                                                        <div class="number">
                                                            5 Estrelas
                                                        </div>
                                                        <div class="progress">
                                                            <div class="progress-bar" style="width: 325%;" role="progressbar" aria-valuenow="325" aria-valuemin="0" aria-valuemax="100">
                                                                325
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="reviews">
                                                        <div class="number">
                                                            4 Estrelas
                                                        </div>
                                                        <div class="progress">
                                                            <div class="progress-bar" style="width: 12%;" role="progressbar" aria-valuenow="12" aria-valuemin="0" aria-valuemax="100">
                                                                12
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="reviews">
                                                        <div class="number">
                                                            3 Estrelas
                                                        </div>
                                                        <div class="progress">
                                                            <div class="progress-bar" style="width: 5%;" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="100">
                                                                5
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="reviews">
                                                        <div class="number">
                                                            2 Estrelas
                                                        </div>
                                                        <div class="progress">
                                                            <div class="progress-bar" style="width: 17%;" role="progressbar" aria-valuenow="17" aria-valuemin="0" aria-valuemax="100">
                                                                17
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="reviews">
                                                        <div class="number">
                                                            1 Estrelas
                                                        </div>
                                                        <div class="progress">
                                                            <div class="progress-bar" style="width: 3%;" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="100">
                                                                3
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="box-comment bg-grey">
                                                    <h2>Wesley Oliveira</h2>
                                                    <p class="star">
                                                        <span><i class="fas fa-star"></i></span>
                                                        <span><i class="fas fa-star"></i></span>
                                                        <span><i class="fas fa-star"></i></span>
                                                        <span><i class="fas fa-star"></i></span>
                                                        <span><i class="fas fa-star"></i></span>
                                                    </p>
                                                    <p>Não sou faixa preta cumpadi, sou preto inteiris, inteiris. Leite de capivaris, leite de mula manquis sem cabeça.</p>
                                                </div>

                                                <div class="box-comment bg-grey">
                                                    <h2>Wesley Oliveira</h2>
                                                    <p class="star">
                                                        <span><i class="fas fa-star"></i></span>
                                                        <span><i class="fas fa-star"></i></span>
                                                        <span><i class="fas fa-star"></i></span>
                                                        <span><i class="far fa-star"></i></span> 
                                                        <span><i class="far fa-star"></i></span> 
                                                    </p>
                                                    <p>Não sou faixa preta cumpadi, sou preto inteiris, inteiris. Leite de capivaris, leite de mula manquis sem cabeça.</p>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="box-sideInfo">
                    <div class="pointcom-line">
                        Carga Horária <span> / 30hrs</span>
                    </div>

                    <div class="pointcom-line">
                        Alunos <span> / 35 </span>
                    </div>

                    <div class="pointcom-line">
                        Avaliações:                         
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="far fa-star"></i>
                        
                    </div>
                    
                </div>

                <a href="#" class="btn-acessar btn-front">
                    Iniciar curso
                </a>

                <div class="box-sideInfo">
                    <h1 class="title-sideInfo">
                        Certificado válido
                    </h1>

                    <img src="assets/img/certificado.jpg" alt="" class="img-fluid">
                </div>

                <div class="box-sideInfo">
                    <h1 class="title-sideInfo">
                        Cursos Relacionados
                    </h1>

                    <!-- LOOP -->
                    <div class="media">
                        <img src="assets/img/curso-teste-single.jpg" class="mr-3" alt="...">
                        <div class="media-body">
                            <h5 class="mt-0">Nome do Curso</h5>
                        </div>
                    </div>
                    <!--// LOOP -->

                    <!-- LOOP -->
                    <div class="media">
                        <img src="assets/img/curso-teste-single.jpg" class="mr-3" alt="...">
                        <div class="media-body">
                            <h5 class="mt-0">Nome do Curso</h5>
                        </div>
                    </div>
                    <!--// LOOP -->

                    <!-- LOOP -->
                    <div class="media">
                        <img src="assets/img/curso-teste-single.jpg" class="mr-3" alt="...">
                        <div class="media-body">
                            <h5 class="mt-0">Nome do Curso</h5>
                        </div>
                    </div>
                    <!--// LOOP -->

                </div>
            </div>
        </div>
</section>



<?php include('footer.php') ?>